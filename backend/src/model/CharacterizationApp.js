const joi = require('joi');

const schema = joi.object().keys({
	id:joi.string().required(),
	id_app:joi.string().required(),
	code_version:joi.string().required(),
	created_at:joi.number().required(),
	started_at:joi.number().required(),
	finished_at:joi.number().required()
});

module.exports = schema;
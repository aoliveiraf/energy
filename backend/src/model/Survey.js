const joi = require('joi');

const schema = joi.object().keys({
	id:joi.string().required(),
	id_app:joi.string().required(),
	list_id_ECC:joi.array().items(joi.string()),
	timeOpen:joi.number().required(),
	timeClose: joi.number().optional(),
	status: joi.string().regex(/open|close/).optional(),
	listECCCollectProperty:joi.array().items(joi.string()),
	listAppCollectProperty:joi.array().items(joi.string()),
	list_calculation:joi.array().items(
		joi.object(
			{
				description:joi.string(),
				level:joi.number(),
				operation:joi.string(),
				basedon:joi.array().items(
					joi.object(
						{
							operation:joi.string(),
							level:joi.number()
						}
		))}))	
});

module.exports = schema;
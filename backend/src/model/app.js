const joi = require('joi');

const schema = joi.object().keys({
	id: joi.string().required(),
	name:joi.string().required(),
	versionDataOutput: joi.string().optional(), // version of the format data output. This property manage to format and quantity of data is return in output. In general put the year.
	type:joi.string().regex(/GPU|CPU/).required(),
	listParamInit: joi.array().items(joi.object()).optional(), // ex. [{energy:{value:50,um:"W"}}]
	model: joi.string().required(),
	listComplement:joi.array().items(joi.object()).optional()
});

module.exports = schema;
const joi = require('joi');

const schema = joi.object().keys({
	time:joi.number().required(),
	id_surveyModel:joi.string().required(),
	type: joi.string().regex(/Collector|Generator/).optional(), //this property has been turning into a arangodb collection
	data: joi.object({}).required(),
	list_property:joi.array.items(joi.object()).required()

});

module.exports = schema;
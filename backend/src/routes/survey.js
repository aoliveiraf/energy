const router = require('express').Router();
const db = require('../config/database');
const db_survey = require('../database/survey');
const collection = "surveyModel";

/* GET home page. */
router.get('/', async (req, res)=> {

  all = await db.collection(collection).all();

  res.json(all._result);
});
/*
* POSTs
*/
 

router.get('/:key',async (req,res)=>{

  const id_surveyModel = `surveyModel/${req.params.key}`;

//  const result = await db_survey.getAppDetail(id_survey)
  const result = await db_survey.getSurvey(id_surveyModel)

  res.status(200).send(result);
})

router.post('/sample', async (req,res)=>{

  const id_survey_model = `surveyModel/${req.body.key_survey_model}`;
  const created_at = req.body.created_at;
  const id_super = `sample/${req.body.key_super}`;
  const level = req.body.level;


  const data ={
    "id_survey_model":id_survey_model,
    "created_at":created_at,
    "read_frequency_ms":req.body.read_frequency_ms,
    "level":level
  }

  if(req.body.key_super != undefined) {
    data.id_super = id_super
  }
  const result = await db_survey.saveSample(data);

  res.status(200).send(result);
})

router.put('/sample/setStartStopTime', async (req,res)=>{

  await db_survey.updateSampleStartStopTime(req.body);

  res.status(200).send({});
})


router.put('/sample/turnIntoPhase1', async (req,res)=>{
//deprecrated
  const key_sample = req.body.key_sample;
  const line = req.body.line;

  const timestamp = new Date(line.split(" ")[0]).getTime()

  const data ={
    "name":"P1",
    "started_at":timestamp,
    "key_sample":key_sample
  }
  const result = await db_survey.updateSampleListPhase(data);


  res.status(200).send(result);
})

router.put('/sample/turnIntoPhase2', async (req,res)=>{
  //deprecrated
    const key_sample = req.body.key_sample;
    const line = req.body.line;
  
    const timestamp = new Date(line.split(" ")[0]).getTime()
  
    const data ={
      "name":"P2",
      "started_at":timestamp,
      "key_sample":key_sample
    }
    const result = await db_survey.updateSampleListPhase(data);
  
  
    res.status(200).send(result);
  })

router.put('/sample/turnIntoPhase3', async (req,res)=>{
//deprecrated
  const key_sample = req.body.key_sample;
  const line = req.body.line;

  const timestamp = new Date(line.split(" ")[0]).getTime()

  const data ={
    "name":"P3",
    "started_at":timestamp,
    "key_sample":key_sample
  }
  const result = await db_survey.updateSampleListPhase(data);


  res.status(200).send(result);
})
router.put('/sample/turnIntoPhase4', async (req,res)=>{
//deprecrated
  const key_sample = req.body.key_sample;
  const line = req.body.line;

  const timestamp = new Date(line.split(" ")[0]).getTime()

  const data ={
    "name":"P4",
    "started_at":timestamp,
    "key_sample":key_sample
  }
  const result = await db_survey.updateSampleListPhase(data);

  res.status(200).send(result);
})  
router.post('/surveyInstance', async (req,res)=>{

  const id_surveyModel = `surveyModel/${req.body.key_surveyModel}`;
  const created_at = req.body.created_at;
  const data ={
    "id_surveyModel":id_surveyModel,
    "created_at":created_at
  }
  const result = await db_survey.saveSurveyInstance(data);

  res.status(200).send(result);
})

router.put('/surveyInstance/finish', async (req,res)=>{

  const finished_at = req.body.finished_at;
  const key_survey_instance = req.body.key_survey_instance;
  const data ={
    "key_survey_instance":key_survey_instance,
    "finished_at":finished_at
  }
  await db_survey.updateSurveyInstanceFinishedAt(data);

  res.status(200).send({});
})


router.put('/sample/finish', async (req,res)=>{

  const finished_at = req.body.finished_at;
  const key_sample = req.body.key_sample;
  const data ={
    "key_sample":key_sample,
    "finished_at":finished_at
  }
  await db_survey.updateSampleFinishedAt(data);

  res.status(200).send({});
})

module.exports = router;
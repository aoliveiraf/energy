const router = require('express').Router();
const db_survey = require('../database/survey');
const db_survey_model = require('../database/surveyModel');
const db_app = require('../database/app');
const db_ecc = require('../database/ecc')

router.put('/addSurveyInstance', async (req, res) => {

  const key_survey_instance = req.body.key_survey_instance;
  const id_survey_model = `surveyModel/${req.body.key_surveyModel}`;
  const index_plan = req.body.index_plan;


  const key_sample = req.body.key_sample;
  const data = {
    "key_survey_instance": key_survey_instance,
    "id_survey_model": id_survey_model,
    "index_plan": index_plan
  }

  await db_survey.addSurveyInstance(data);

  res.status(200).send({});
})

router.delete('/:key_survey_model',async (req, res) =>{
  
  const id_survey_model = `surveyModel/${req.params.key_survey_model}`;

  await db_survey_model.deleteAllSample(id_survey_model);

  res.status(200).send({});

})

router.put('/prepareForRun/:key_survey_model', async (req, res) => {

  const id_survey_model = `surveyModel/${req.params.key_survey_model}`;

  const survey_model = await db_survey.getSurvey(id_survey_model)

  const plan = survey_model.plan;

  const res_plan = {}
  let generator_detail = {}

  if(plan.generator != undefined) {

    const generator = plan.generator;

    const key_app = survey_model.generator.ref;

    const output_version = survey_model.generator.output_version;
    
    const app = await db_app.get({"key_app":key_app});

    if(app.list_output_version[output_version].p3 != undefined) {

      generator_detail.p3 ={};
      
      generator_detail.p3.start_term = app.list_output_version[output_version].p3.start_term;
      generator_detail.p3.finish_term = app.list_output_version[output_version].p3.finish_term;

    }
    let container = Object.assign({}, survey_model.generator.container);
  
    generator_detail.container=container;
    generator_detail.route = app.list_output_version[output_version].route;
    generator_detail.list_operation = generator.list_operation;
    generator_detail.send_data = generator.send_data;


    res_plan.generator=generator_detail;

  }
  let list_frequency = []

  let list_container = []

  const collector_detail = {}


  if (plan.collector != undefined) {

    const collector = plan.collector;

    const frequency = collector.read_frequency_ms;
    // *** Frequency ***
    const add_list = frequency.add;
    const del_list = frequency.del;

    let f = parseInt(frequency.min_value);
    const limit = parseInt(frequency.repeat);
    const interval = parseInt(frequency.interval)
    const list_property = []
    //collector.list_default_property.map((element) => { return element });
    for (let i = 0; i < limit; i++) {

      if (del_list.indexOf(f) == -1) {

        while (add_list.length > 0) {

          const first_element = parseInt(add_list[0]);

          if (first_element < f) {

            list_frequency.push(first_element);
            add_list.shift();

          } else {
            break;
          }
        }
        list_frequency.push(f);
      }
      f = f + interval;
    }
    for (let k = 0; k < add_list.length; k++) {
      list_frequency.push(add_list[k]);
    }
    //** Frequency */

    //**input */

    input_output = collector.input_output;

    list_input_output = input_output.add.map((element) => { return element });

    for(let i=0;i<list_input_output.length;i++) {

      list_property.push(list_input_output[i]);
    }

    for (let k = 0; k < list_frequency.length; k++) {

      let container = Object.assign({}, survey_model.collector.container);
      
      let command = []
      if (container.command != undefined) {

        command =survey_model.collector.container.command.map((element)=>{return element})

      }

      command.push("--query-gpu=" + list_input_output.toString());
      command.push("--loop-ms=" + list_frequency[k]);
      command.push("--format=csv")

      container.read_frequency_ms = list_frequency[k];
      container.command = command;
      container.list_property = list_property;

      list_container.push(container);

    }

    const key_app = survey_model.collector.ref;

    const output_version = survey_model.collector.output_version;
    
    const app = await db_app.get({"key_app":key_app});

    console.log(app.list_output_version[output_version].route);

    collector_detail.list_frequency=list_frequency;
    collector_detail.list_container=list_container;
    collector_detail.list_device=survey_model.collector.list_device;
    collector_detail.route = app.list_output_version[output_version].route;
    collector_detail.list_operation = collector.list_operation;
    collector_detail.send_data = collector.send_data;


    /**
     * container detail for user property
     */

    container = Object.assign({}, survey_model.collector.container);

    collector_detail.list_container_user_property =[]
    
    for(let i=0;i<collector_detail.list_device.length;i++) { 
      
      const ecc = await db_ecc.get({"key_ecc":collector_detail.list_device[i]})

      let command = []

      if (container.command != undefined) {

        command =survey_model.collector.container.command.map((element)=>{return element})

      }

      command.push("--query-gpu=" + ecc.list_user_property.toString());
      command.push("--format=csv")
      

      container.command = command;
      container.key_ecc = ecc._key;
      container.list_property = ecc.list_user_property
      collector_detail.list_container_user_property.push(container)
    }
    
    res_plan.collector=collector_detail;

    
  }//end collector
  res_plan.num_level = survey_model.num_level;
  res_plan.batch_size=plan.batch_size;
  res_plan.sleep=plan.sleep;
  res_plan.stop=plan.stop;
  res_plan.repeat=plan.repeat;
  
  const data = {
    "id_survey_model":id_survey_model,
    "run_plan":res_plan
  }
  await db_survey_model.updateSurveyModelRunPlan(data) 

  res.status(200).send(res_plan);
})


module.exports = router;
const router = require('express').Router();
const db = require('../config/database');
const db_ecc = require('../database/ecc');

/* GET home page. */
router.get('/', async (req, res)=> {

  all = await db.collection(collection).all();

  res.json(all._result);
});
/*
* POSTs
*/

router.put('/turnECCIntoPhase0', async (req,res)=>{

  const list_device = req.body.list_device;
  const time_ms = req.body.time_ms;

  for(let i=0;i<list_device.length;i++) {

    const data ={
      "key_ecc":list_device[i].id,
      "time_ms":time_ms,
      "this":req.body.this
    }
    await db_ecc.turnECCIntoPhase0(data);
  
  }
  
  res.status(200).send({});
})
module.exports = router;
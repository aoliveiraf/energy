const router = require('express').Router();
const db = require('../../../config/database')
const db_survey = require('../../../database/survey');
const db_primary_ecc = require('../../../database/primaryECC')
const db_calculation_si = require('../../../database/calculationSurveyInstance')
const db_calculation = require('../../../database/calculation')

const System = require('../../../model/System')


router.put('/secondary/energy/average/:key_survey_model', async (req, res) => {

    /**
     *           {
      "key_sample": "30751107",
      "data_send":{
      "basedon":"power.draw",
      "route_average":"/canonicalOperation/secondaryECC/average",
      "base_phase":"P3"
      }
     */
    let id_survey_model = `surveyModel/${req.params.key_survey_model}`;

    let list_id = await db.query(`for s in sample filter s.id_survey_model == @id_survey_model and s.level == 2
    return s._id`, { id_survey_model })

    const list_id_sample = list_id._result;

    for (let i = 0; i < list_id_sample.length; i++) {

        const id_sample = list_id_sample[i];
        const basedon = req.body.data_send.basedon;
        const route_average = req.body.data_send.route_average;
        const base_phase = req.body.data_send.phase.toUpperCase();


        const list_energy = await db_calculation.getDataForLevel2Average({ id_sample: id_sample, route: route_average, basedon: basedon, phase: base_phase });

        let sum = 0.0;
        for (let k = 0; k < list_energy.length; k++) {

            sum += list_energy[k];
        }


        let energy = Math.round(sum / list_energy.length);

        const operation = {
            route: "/canonicalOperation/secondaryECC/energy",
            value: energy,
            unit: "Joule",
            basedon: basedon
        }

        //  await db_survey.updateSurveyInstanceListOperation(id_sample, operation)

        res.status(200).send(operation);

    }
})


router.put('/primary/energy/:key_survey_model', async (req, res) => {

    /**
     * "data_send":{
     *  "phase":"P3",
     *  "route_average":"/canonicalOperation/primaryECC/average",
     *  "route_time":"/canonicalOperation/primaryECC/duration",
     *  "basedon":"power.draw"
     * }
     */
    let id_survey_model = `surveyModel/${req.params.key_survey_model}`;

    let list_id = await db.query(`for s in sample filter s.id_survey_model == @id_survey_model and s.level == 1
    return s._id`, { id_survey_model })

    const list_id_sample = list_id._result;

    for (let i = 0; i < list_id_sample.length; i++) {

        const id_sample = list_id_sample[i];

        const basedon = req.body.data_send.basedon;
        const route_average = req.body.data_send.route_average;
        const route_time = req.body.data_send.route_time;
        const base_phase = req.body.data_send.phase.toUpperCase();

        const average_power = await db_calculation_si.getCanonicalDataLevel3({ id_sample: id_sample, route: route_average, basedon: basedon, phase: base_phase });

        const duration = await db_calculation_si.getCanonicalDataLevel3({ id_sample: id_sample, route: route_time, basedon: "timestamp", phase: base_phase });

        let energy = Math.round(duration * average_power);

        const operation = {
            route: "/canonicalOperation/primaryECC/energy",
            list_result: [
                {
                    value: energy,
                    unit: "Joule",
                    phase: base_phase,
                    basedon: basedon
                }
            ]
        }

        console.log(operation);
        

        await db_survey.updateSurveyInstanceListOperation(id_sample, operation)

    }

    res.status(200).send({});


})


router.put('/primary/average/:key_survey_model', async (req, res) => {


    let id_survey_model = `surveyModel/${req.params.key_survey_model}`;

    let list_id = await db.query(`for s in sample filter s.id_survey_model == @id_survey_model and s.level == 1
    return s._id`, { id_survey_model })



    const list_id_sample = list_id._result;

    console.log(list_id_sample);

    for (let i = 0; i < list_id_sample.length; i++) {

        const id_sample = list_id_sample[i];

        const list_property = req.body.data_send.list_property;
        const phase_filter = req.body.data_send.phase.toUpperCase();

        const next_phase_name = System.list_phase[phase_filter.toUpperCase()].next

        const phase_start = await db_survey.getPhase(id_sample, phase_filter.toUpperCase())
        const phase_finish = await db_survey.getPhase(id_sample, next_phase_name)



        const limit = {
            "started_at": phase_start.started_at,
            "finished_at": phase_finish.started_at
        }

        console.log(limit);

        const list = await db_survey.getAllSampleProperty(id_sample, limit);

        console.log(list);

        let list_result = [];
        for (let i = 0; i < list_property.length; i++) {
            let sum = 0.0;
            for (let j = 0; j < list.length; j++) {

                sum = sum + parseFloat(list[j][list_property[i]]);
            }
            const average = sum / list.length;
            list_result.push({ value: average, basedon: list_property[i], phase: phase_filter, unit: "Watts" })


        }
        const operation = {
            route: "/canonicalOperation/primaryECC/average",
            list_result: list_result
        }
        console.log(operation);

        await db_survey.updateSampleListOperation(id_sample, operation)
    }

    res.status(200).send({});

})

router.put('/primary/standardDeviation/:key_survey_model', async (req, res) => {

    let id_survey_model = `surveyModel/${req.params.key_survey_model}`;

    let list_id = await db.query(`for s in sample filter s.id_survey_model == @id_survey_model and s.level == 1
    return s._id`, { id_survey_model })



    const list_id_sample = list_id._result;

    console.log(list_id_sample);

    for (let i = 0; i < list_id_sample.length; i++) {

        const id_sample = list_id_sample[i];
        const list_operation = req.body.data_send.list_operation;
        const phase_filter = req.body.data_send.phase.toUpperCase();

        const next_phase_name = System.list_phase[phase_filter].next;

        const phase_start = await db_survey.getPhase(id_sample, phase_filter.toUpperCase())
        const phase_finish = await db_survey.getPhase(id_sample, next_phase_name)

        const limit = {
            "started_at": phase_start.started_at,
            "finished_at": phase_finish.started_at
        }

        console.log(limit);

        let list_result = [];
        for (let i = 0; i < list_operation.length; i++) {
            const route_average = list_operation[i].route_average;
            list_basedon = list_operation[i].list_basedon;

            for (let j = 0; j < list_basedon.length; j++) {
                const basedon = list_basedon[j];

                const average = await db_calculation_si.getCanonicalDataLevel3({ id_sample: id_sample, route: route_average, basedon: basedon, phase: phase_filter });
                console.log(average);
                
                const list_value = await db_primary_ecc.getPropertyValueByPropertyName(id_sample, basedon, limit);

                let sum = 0.0;
                for (let k = 0; k < list_value.length; k++) {

                    const diff = parseFloat(list_value[k]) - parseFloat(average);

                    sum = sum + Math.pow(diff, 2);
                }

                const average_v = sum / list_value.length;

                const st = Math.sqrt(average_v)

                list_result.push({ value: st, basedon: basedon, phase: phase_filter, unit: "Watts" })

            }
        }
        const operation = {
            route: "/canonicalOperation/primaryECC/standardDeviation",
            list_result: list_result
        }
        console.log(operation);
        
      //  await db_survey.updateSurveyInstanceListOperation(id_sample, operation)
    }
    res.status(200).send({});

})

router.put('/primary/duration/:key_survey_model', async (req, res) => {

    /**
     *           {
            "data_send": {
                "phase":"P3"
            }
          }
     */
    let id_survey_model = `surveyModel/${req.params.key_survey_model}`;

    let list_id = await db.query(`for s in sample filter s.id_survey_model == @id_survey_model and s.level == 1
    return s._id`, { id_survey_model })


    const list = list_id._result;
    for (let i = 0; i < list.length; i++) {

        const id_sample = list[i];

        const base_phase = req.body.data_send.phase.toUpperCase();

        const next_phase_name = System.list_phase[base_phase].next

        const phase_start = await db_survey.getPhase(id_sample, base_phase);
        const phase_finish = await db_survey.getPhase(id_sample, next_phase_name)


        const diff = phase_finish.started_at - phase_start.started_at;

        let spent_time = Math.round(diff / 1000);

        const operation = {
            route: "/canonicalOperation/primaryECC/duration",
            list_result: [
                {
                    phase: base_phase,
                    unit: "Second",
                    value: spent_time,
                    basedon: "timestamp"
                }
            ]
        }

        console.log(operation);
        
        await db_survey.updateSurveyInstanceListOperation(id_sample, operation)

    }

    res.status(200).send({});


})



module.exports = router;
const router = require('express').Router();
const db_survey = require('../../../database/survey');
const db_calculation = require('../../../database/calculation')
const db_calculation_si = require('../../../database/calculationSurveyInstance')


router.put('/canonical/speed/sum', async (req, res) => {

    const id_sample = `sample/${req.body.key_sample}`;


    res.status(200).send({});

})

router.put('/canonical/imageModel/size/sum', async (req, res) => {

    const id_sample = `sample/${req.body.key_sample}`;

    res.status(200).send({});
})

module.exports = router;
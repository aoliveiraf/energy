const router = require('express').Router();
const db_survey = require('../../../database/survey');
const db_calculation = require('../../../database/calculation')
const db_calculation_si = require('../../../database/calculationSurveyInstance')
const System = require('../../../model/System')
const db_survey_model = require('../../../database/surveyModel')


router.put('/energy/average', async (req, res) => {

  /**
   *           {
	"key_sample": "30751107",
	"data_send":{
	"basedon":"power.draw",
	"route_average":"/canonicalOperation/secondaryECC/average",
	"base_phase":"P3"
	}
   */
  const id_sample = `sample/${req.body.key_sample}`;
  const basedon = req.body.data_send.basedon;
  const route_average = req.body.data_send.route_average;
  const base_phase = req.body.data_send.phase.toUpperCase();

  const list_energy = await db_calculation_si.getCanonicalDataLevel3({id_sample:id_sample, route:route_average, basedon:basedon,phase:base_phase});

  const list_spent_time = await db_survey_model.getUpLevelValue({})

  let sum = 0.0;
  for (let i = 0; i < list_spent_time.length; i++) {

    sum += list_spent_time[i];
  }

  let average_spent_time_ms = sum / list_spent_time.length;

  let average_spent_time_s = Math.round(average_spent_time_ms / 1000);

  let energy = average_spent_time_s * average_power;

  const operation = {
    route: "/canonicalOperation/secondaryECC/energy/everage",
    value: energy,
    unit:"second"
  }

//  await db_survey.updateSurveyInstanceListOperation(id_sample, operation)

  res.status(200).send(operation);


})


router.put('/average', async (req, res) => {

  /**
   * {
          "description": "This route calculates the average ...",
          "route": "/canonicalOperation/secondaryECC/average",
          "data_send": {
            "phase":"P3",
            "list_operation": [
              {
                "route": "/canonicalOperation/sampleECC/sum",
                "list_basedon": [
                  "power.draw"
                ]
              },
              {
                "route": "/canonicalOperation/sampleECC/average",
                "list_basedon": [
                  "pcie.link.gen.current",
                  "pcie.link.width.current",
                  "memory.used",
                  "memory.free",
                  "utilization.gpu",
                  "clocks.current.graphics",
                  "clocks.current.sm",
                  "clocks.current.memory"
                ]
              }
            ]
          }
        },
   */
  const id_sample = `sample/${req.body.key_sample}`;
  const list_operation = req.body.data_send.list_operation;
  const base_phase = req.body.data_send.phase.toUpperCase();

  let list_result = [];
  for (let i = 0; i < list_operation.length; i++) {
    const route = list_operation[i].route;
    list_basedon = list_operation[i].list_basedon;

    for (let j = 0; j < list_basedon.length; j++) {
      const basedon = list_basedon[j];

      const list_value = await db_calculation.getDataForLevel2Average({id_sample:id_sample, route:route, basedon:basedon,phase:base_phase});

      let sum = 0.0;
      for (let k = 0; k < list_value.length; k++) {

        sum = sum + parseFloat(list_value[k]);
      }
      const average = sum / list_value.length;

      list_result.push({ value: average, basedon: basedon,phase:base_phase })

    }
  }
  const operation = {
    route: "/canonicalOperation/secondaryECC/average",
    list_result: list_result
  }

  await db_survey.updateSurveyInstanceListOperation(id_sample, operation)

  res.status(200).send({});

})


router.put('/standardDeviation', async (req, res) => {

  /**
   * ,
        {
          "description": "This route calculates the standard deviation ...",
          "route": "/canonicalOperation/secondaryECC/standardDeviation",
          "data_send": {
            "list_operation": [
              {
                "route_average": "/canonicalOperation/surveyECC/average",
                "route_base": "/canonicalOperation/sampleECC/average",
                "list_basedon": [
                  "pcie.link.gen.current",
                  "pcie.link.width.current",
                  "memory.used",
                  "memory.free",
                  "utilization.gpu",
                  "clocks.current.graphics",
                  "clocks.current.sm",
                  "clocks.current.memory"
                ]
              },
              {
                "route_base": "/canonicalOperation/sampleECC/sum",
                "route_average": "/canonicalOperation/surveyECC/average",
                "list_basedon": [
                  "power.draw"
                ]
              }
            ]
          }
        }
   */
  console.log("/standardDeviation");


  const id_sample = `sample/${req.body.key_sample}`;
  const list_operation = req.body.data_send.list_operation;
  const base_phase = req.body.data_send.phase.toUpperCase();


  let list_result = [];
  for (let i = 0; i < list_operation.length; i++) {
    const route_average = list_operation[i].route_average;
    const route_base = list_operation[i].route_base;
    list_basedon = list_operation[i].list_basedon;

    for (let j = 0; j < list_basedon.length; j++) {
      const basedon = list_basedon[j];

      const average = await db_calculation_si.getCanonicalDataLevel3({id_sample:id_sample, route:route_average, basedon:basedon,phase:base_phase});

      const list_value = await db_calculation.getDataForLevel2Average({id_sample, route_base, basedon,phase:base_phase});

      let sum = 0.0;
      for (let k = 0; k < list_value.length; k++) {

        const diff = parseFloat(list_value[k]) - parseFloat(average);

        sum = sum + Math.pow(diff, 2);
      }

      const average_v = sum / list_value.length;

      const st = Math.sqrt(average_v)

      list_result.push({ value: st, basedon: basedon })

    }
  }
  const operation = {
    route: "/canonicalOperation/secondaryECC/standardDeviation",
    list_result: list_result
  }

  await db_survey.updateSurveyInstanceListOperation(id_sample, operation)

  res.status(200).send({});

})


module.exports = router;
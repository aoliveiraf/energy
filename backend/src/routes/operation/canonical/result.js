const router = require('express').Router();
const db_survey = require('../../../database/survey');
const db_primary_ecc = require('../../../database/primaryECC')
const db_result = require('../../../database/result')
const db_calculation_si = require('../../../database/calculationSurveyInstance')
const db_survey_model = require('../../../database/surveyModel')

router.put('/:level/greaterStandardDeviation', async (req, res) => {

    /**
     * Depends on the average.
     */
    const id_sample = `sample/${req.body.key_sample}`;
    const limit = parseFloat(req.body.data_send.limit_number_less_std);
    const basedon = req.body.data_send.basedon;

    req.body.route = "/canonicalOperation/result/greaterStandardDeviation";
    let abort = false;

    const sample = await db_survey_model.getSample(req.body.key_sample)

    console.log(req.body);
    
    let current_result = await db_result.getResult(sample.id_super, req.body.route);

    const std = await db_calculation_si.getCanonicalDataLevel3(id_sample, req.body.data_send.route, basedon);

    console.log(current_result);
    
    if (current_result[req.body.route] == null) {
        current_result = {
        }
        current_result[req.body.route] = {
            "route": req.body.data_send.route,
            "greater_std": {
                "value": parseFloat(std),
                "key_sample": req.body.key_sample
            },
            "number_less_greater":0 

        }


    } else if (parseFloat(std) <= parseFloat(current_result[req.body.route].greater_std.value)) {
        
        current_result[req.body.route].number_less_greater +=1;
        
        if (limit  <= parseFloat(current_result[req.body.route].number_less_greater) && req.body.data_send.abort == true) {
            abort = true;
        }
    } else {

        current_result[req.body.route].greater_std.value = std;
        current_result[req.body.route].number_less_greater =0;
        current_result[req.body.route].greater_std.key_sample = req.body.key_sample;        
    }

    await db_result.updateResult(sample.id_super, current_result);

    res.status(200).send({ "abort": abort });

})


module.exports = router;
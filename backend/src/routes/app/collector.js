const router = require('express').Router();
const db_survey = require('../../database/survey');
const util = require('../../model/Util')

/* GET home page. */
router.get('/', async (req, res)=> {

  all = await db.collection(collection).all();

  res.json(all._result);
});
/*
* The path is formad by the year of the output format followed by a sequence in this year.
*
* nvidia-smi --query-gpu=power.draw,pcie.link.gen.current,pcie.link.width.current,memory.used,memory.free,utilization.gpu,clocks.current.graphics,clocks.current.sm,clocks.current.memory --format=csv,noheader --loop-ms=1000
*/
 
router.post('/2020_1',async (req,res)=>{ 

  const body = req.body;

  const id_surveyModel = `surveyModel/${body.key_surveyModel}`;
  const id_sample = `sample/${body.key_sample}`;
  const list_property = body.list_property

//  const surveyModel = await db_survey.getSurvey(id_surveyModel);
  
//  console.log(body.batch)

  console.log(list_property);
  
  const list_json = util.semiCsvToJSON(body.batch,list_property)

//  console.log(list_json);

  for(let i=0;i<list_json.length;i++) {

    const data = {
      id_surveyModel:id_surveyModel,
      id_sample:id_sample,
      timestamp:list_json[i].timestamp,
      list_property:list_json[i]
  
    }
    await db_survey.saveSampleECCItem(data)
  
  }
  
  res.status(200).send(list_json);

})
//doing
router.post('/nvidiaGPUCollector_1/2020_1/userProperty',async (req,res)=>{ 

  const data = {

    key_sample:req.body.key_sample,
    row:req.body.row
  }


  res.status(200).send({});

})


module.exports = router;
const router = require('express').Router();
const db_survey = require('../../database/survey');


const util = require('../../model/Util')
const collection = "sampleAppItem";


/* GET home page. */
router.get('/', async (req, res)=> {

  all = await db.collection(collection).all();

  res.json(all._result);
});
/*
* The path is formad by the year of the output format followed by a sequence in this year.
*/
 
router.post('/2012_1',async (req,res)=>{ 

  const body = req.body;

//  console.log(body);

  const id_surveyModel = `surveyModel/${body.key_surveyModel}`;

  let batch = body.batch;
  
  for(let i=0;i<batch.length;i++) {

    let row = batch[i];

    let split = row.split(" ")

    const timestamp = new Date(split[0]).getTime();

    const image_full_path = split[2];

    row = row.replace(timestamp,"");
    
    split = row.split(":")

    const list_image_classify = util.splitRowCSV(split[1]);
  
    const data = {
  
      id_surveyModel:id_surveyModel,
      list_property:{
        timestamp:timestamp,
        image_full_path:image_full_path,
        list_image_classify:list_image_classify
      }
    }

    await db_survey.saveSampleAppItem(data)

  }


  res.status(200).send(body);

})

router.put('/close/:id',async (req,res)=>{

  const id = req.params.id;

  const collect = await db.collection(collection).firstExample({id:id});

  const _collect = await db.collection(collection).updateByExample({_key:collect._key},{status:"close",timeClose:new Date().getTime()});

  res.status(200).send(_collect)
})

router.get('/open',async (req,res)=>{

  const all = await db.collection(collection).byExample({status:"open"}) 

  res.status(200).send(all._result)
})

module.exports = router;
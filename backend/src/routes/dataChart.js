const router = require('express').Router();

router.get('/freq_vs_std', async (req, res) => {

    let data = [
            [10,20,30,40,50],
            [2.9,3.9,4.9,5.9,6.0]
        ]
    res.status(200).send({ data });

})

router.get('/freq_vs_power_draw', async (req, res) => {

    let data = [
            [10,20,30,40,50],
            [36.1,35.4,35.1,34.9,35.0]
        ]
    res.status(200).send({ data });

})

module.exports = router;
const db = require('../config/database')
const collection = "surveyModel";
module.exports = {
    save: function (data) {
        db.collection(collection).save(data);
    },
    getAppDetail: async function (id_surveyModel) {
        const result = await db.query(`
        for s in surveyModel filter s._id == @id_surveyModel 
        return {"gen":{"list_argument":s.genarator_app_detail.ref_app.protocol_detail.list_argument,
                       "route":s.genarator_app_detail.route,
                       "container_name":s.genarator_app_detail.container.name},
                "coll":{"list_argument":s.collector_app_detail.ref_app.protocol_detail.list_argument,
                       "route":s.collector_app_detail.route,
                       "container_name":s.collector_app_detail.container.name}}
        `,
            { id_surveyModel: id_surveyModel })

        return result._result[0];
    },
    getSurvey: async function (id_surveyModel) {

        const result = await db.query(`
        for s in surveyModel filter s._id == @id_surveyModel 
        return s
        `,
            { id_surveyModel: id_surveyModel })

        return result._result[0];

    },
    saveSampleECCItem: async function (data) {

        await db.collection("sampleECCItem").save(data);
    },
    saveSampleAppItem: async function (data) {

        await db.collection("sampleAppItem").save(data);
    },
    saveSurveyInstance: async function (data) {

        const result = await db.collection("surveyInstance").save(data);

        return result;
    },
    updateSurveyInstance: async function (data) {

        const result = await db.collection("surveyInstance").update(data);

        return result;
    },

    saveSample: async function (data) {

        const result = await db.collection("sample").save(data);

        return result;
    },
    updateSampleFinishedAt: async function (data) {

        const id_sample = `sample/${data.key_sample}`
        await db.query(`
            for s in sample filter s._id == @id_sample
            update s with {finished_at:@finished_at} in sample
            
        `, { id_sample: id_sample, finished_at: data.finished_at })
    },
    updateSurveyInstanceFinishedAt: async function (data) {

        const id_survey_instance = `surveyInstance/${data.key_survey_instance}`

        await db.query(`
            for s in surveyInstance filter s._id == @id_survey_instance
            update s with {finished_at:@finished_at} in surveyInstance
        `, { id_survey_instance: id_survey_instance, finished_at: data.finished_at })

    },
    updateSampleListOperation: async function (id_sample, operation) {

        await db.query(`
        for s in sample filter s._id == @id_sample 
        update s with {list_operation:PUSH(s.list_operation,@operation)} in sample
    `, { id_sample: id_sample, operation: operation })
        // await db.collection("sample").update(key_sample,data);

    },
    getAllSampleProperty: async function (id_sample,limit) {

        const result = await db.query(`
                FOR s IN sampleECCItem FILTER s.id_sample == @id_sample and s.timestamp >= @limit.started_at and s.timestamp <=@limit.finished_at 
                RETURN s.list_property
             `,
            { id_sample: id_sample,limit:limit })
        
        return result._result;
    },
    updateSurveyInstanceListOperation: async function (id_sample, operation) {

        await db.query(`
        for s in sample filter s._id == @id_sample 
        update s with {list_operation:PUSH(s.list_operation,@operation)} in sample
    `, { id_sample: id_sample, operation: operation })

    },
    updateSampleListPhase: async function (data) {

        const id_sample = `sample/${data.key_sample}`;
        delete data.key_sample;

        await db.query(`
            for s in sample filter s._id == @id_sample
            update s with {list_phase:push(s.list_phase,@data)} in sample
            
        `, { id_sample: id_sample, data: data })
    },
    getPhase: async function (id_sample, phase_name) {

        const result = await db.query(`for s in sample filter s._id == @id_sample
                    for p in s.list_phase filter p.name ==@phase_name return p`,
                    {id_sample:id_sample,phase_name:phase_name})

        return result._result[0]
    },
    updateSampleStartStopTime: async function (data) {

        const id_sample = `sample/${data.key_sample}`;
        delete data.key_sample;

        await db.query(`for s in sample filter s._id == @id_sample update s with @data in sample`, { id_sample: id_sample, data: data })
    },
    getListSpentTime:async function(data) {

        const result = await db.query(`for s in sample filter s.id_super == @data.id_super
        let started_at = (for p in s.list_phase filter p.name ==@data.base_phase return p.started_at)
        let finished_at = (for p in s.list_phase filter p.name ==@data.next_phase return p.started_at)
        return finished_at - started_at`,{data})

        return result._result
        
    }

}

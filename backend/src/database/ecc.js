const db = require('../config/database')

module.exports = {
    turnECCIntoPhase0: async function (data) {

        const id_ecc = `ECCSystem/${data.key_ecc}`
        
        await db.query(`
        for s in ECCSystem filter s._id == @id_ecc
        update s with {"status":{"ecc_phase_0":{"at":@time_ms,"this":@_this}}} in ECCSystem
                     `,
            {id_ecc:id_ecc,time_ms:data.time_ms,_this:data.this})

    },
    get: async function (data) {

        const id_ecc = `ECC/${data.key_ecc}`
        
        const result = await db.query(`
        for s in ECC filter s._id == @id_ecc return s
                     `,
            {id_ecc:id_ecc})


        return result._result[0]
    }

}

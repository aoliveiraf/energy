const db = require('../config/database')

module.exports = {
    getDataForLevel2Average: async function (data) {
        console.log("getDataForLevel2Average");
        
        const result = await db.query(`
            for s in sample filter s.id_super == @data.id_sample
                for op in s.list_operation filter op.route == @data.route 
                    for re in op.list_result filter re.basedon == @data.basedon
                        return re.value
                 `,
            {data:data})

        return result._result;
    }

}

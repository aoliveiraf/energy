const db = require('../config/database')

module.exports = {
    get: async function (data) {

        const id_app = `app/${data.key_app}`
        
        const result = await db.query(`
        for a in app filter a._id == @id_app
        return a                     `,
            {id_app:id_app})

    return result._result[0];

    }


}

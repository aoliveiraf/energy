const db = require('../config/database')
const collection = "surveyModel";
module.exports = {
    deleteAllSample: async function (id_survey_model) {
        console.log("deleteAllSample: " + id_survey_model);

        await db.query(`
        for s in sample filter s.id_survey_model == @id_survey_model 
        remove s in sample
        for item in sampleECCItem filter item.id_sample == s._id
        remove item in sampleECCItem
        for item2 in sampleAppItem filter item.id_sample == s._id
        remove item2 in sampleAppItem`, { id_survey_model: id_survey_model });
    },
    addSurveyInstanceOnRunPlan: async function (data) {
        const id_survey_model = `surveyModel/${data.key_survey_model}`;

        await db.query(`for sm in surveyModel filter sm._id == @id_survey_model

        let new_plan =  PUSH(REMOVE_NTH(sm.run_plan, @index_plan),MERGE(sm.run_plan[@index_plan],{"key_survey_instance":@key_survey}))
        update sm with {run_plan:new_plan} in surveyModel
        `, { id_survey_model: id_survey_model, key_survey: data.key_survey_instance, index_plan: data.index_plan })
    },
    addIndexStartSampleOnRunPlanColl: async function (data) {
        await db.query(`    addInstance: function (data) {
            for sm in surveyModel filter sm._id == @id_survey_model
        
        let coll = MERGE(sm.run_plan[@index_plan].coll,{"index_start_sample":@index_start_sample})
        
        let unset_coll = UNSET(sm.run_plan[@index_plan],"coll")
        
        let new_plan = MERGE(UNSET(sm.run_plan[@index_plan],"coll"),{"coll":coll})
        
        let new_run = PUSH(REMOVE_NTH(sm.run_plan, @index_plan),new_plan)
        
        update sm with {run_plan:new_run} in surveyModel let NEW =NEW
                
        return NEW
        
        `, { id_survey_model: id_survey_model, index_plan: data.index_plan, index_start_sample: data.index_start_sample })

    },
    addIndexFinishSampleOnRunPlanColl: async function (data) {

        await db.query(`    addInstance: function (data) {
            for sm in surveyModel filter sm._id == @id_survey_model
        
        let coll = MERGE(sm.run_plan[@index_plan].coll,{"index_finish_sample":@index_finish_sample})
        
        let unset_coll = UNSET(sm.run_plan[@index_plan],"coll")
        
        let new_plan = MERGE(UNSET(sm.run_plan[@index_plan],"coll"),{"coll":coll})
        
        let new_run = PUSH(REMOVE_NTH(sm.run_plan, @index_plan),new_plan)
        
        update sm with {run_plan:new_run} in surveyModel let NEW =NEW
                
        return NEW
        
        `, { id_survey_model: id_survey_model, index_plan: data.index_plan, index_finish_sample: data.index_finish_sample })

    },
    addIndexStartSampleOnRunPlanGen: async function (data) {
        await db.query(`
        addInstance: function (data) {
            for sm in surveyModel filter sm._id == @id_survey_model
        
        let gen = MERGE(sm.run_plan[@index_plan].gen,{"index_start_sample":@index_start_sample})
        
        let unset_gen = UNSET(sm.run_plan[@index_plan],"gen")
        
        let new_plan = MERGE(UNSET(sm.run_plan[@index_plan],"gen"),{"gen":gen})
        
        let new_run = PUSH(REMOVE_NTH(sm.run_plan, @index_plan),new_plan)
        
        update sm with {run_plan:new_run} in surveyModel let NEW =NEW
                
        return NEW
        
        `, { id_survey_model: id_survey_model, index_plan: data.index_plan, index_start_sample: data.index_start_sample })

    },
    addIndexFinishSampleOnRunPlanGen: async function (data) {

        await db.query(`
        addInstance: function (data) {
            for sm in surveyModel filter sm._id == @id_survey_model
        
        let gen = MERGE(sm.run_plan[@index_plan].gen,{"index_finish_sample":@index_finish_sample})
        
        let unset_gen = UNSET(sm.run_plan[@index_plan],"gen")
        
        let new_plan = MERGE(UNSET(sm.run_plan[@index_plan],"gen"),{"gen":gen})
        
        let new_run = PUSH(REMOVE_NTH(sm.run_plan, @index_plan),new_plan)
        
        update sm with {run_plan:new_run} in surveyModel let NEW =NEW
                
        return NEW
        
        `, { id_survey_model: id_survey_model, index_plan: data.index_plan, index_finish_sample: data.index_finish_sample })

    },
    remove_all_survey_instance: async function (id_survey_model) {

        await db.query(`for si in surveyInstance filter si.id_surveyModel == @id_survey_model
                    remove si in surveyInstance
                    for sa in sample filter sa.id_surveyInstance == si._id
                remove sa in sample
         `, { id_survey_model: id_survey_model })
    },
    remove_last_sample: async function (id_survey_instance) {

        await db.query(`    
                        for sa in sample filter sa.id_surveyInstance == @id_survey_instance  sort sa.created_by DESC LIMIT 1
                            remove sa in sample
                `, { id_survey_instance: id_survey_instance })

    },
    getCaracterizationSurvey() {

    },
    prepareForRunPlan: async function (data) {

        const id_survey_model = `surveyModel/${data.key_survey_model}`;
        const action = data.action;

        survey = this.getCaracterizationSurvey

        if (survey == undefined) {

            createCaracterization
        }
        if (action == "reset") {

            this.remove_all_survey_instance(id_survey_model)

        } else {

            const surveyModel = db_survey.getSurveyModel(id_survey_model);

            const plan = surveyModel.run_plan[data.index_plan];

            if (plan.key_survey_instance != undefined) {

                id_survey_instance = plan.key_survey_instance;

                if (plan[data.index_plan].coll.index_start_sample > plan[data.index_plan].coll.index_finish_sample) {

                    this.remove_last_sample(id_survey_instance);
                }

            }

        }

    },
    updateSurveyModelRunPlan: async function (data) {

        const id_survey_model = data.id_survey_model;
        await db.query(`for sm in surveyModel filter sm._id == @id_survey_model
        update sm with {"run_plan":@run_plan} in surveyModel`, { id_survey_model: id_survey_model, run_plan: data.run_plan })

    },
    getSample: async function (key_sample) {
        console.log(key_sample);

        return await db.collection("sample").firstExample({ _id: `sample/${key_sample}` });
    },

    getUpLevelValue: async function (data) {

        data.id_survey_model = `surveyModel/${data.key_survey_model}`;

        const result = await db.query(`
        let list_id_super = (for ss in sample filter ss.id_survey_model == @data.id_survey_model and HAS(ss,"list_operation")
        for op in ss.list_operation filter op.route == @data.route
        for r in op.list_result filter r.basedon == @data.basedon
        for s in sample filter has(s,"id_super") and s.id_super == ss._id and s.level == @data.base_level
        return DISTINCT ss._id)
        
        for ss in sample filter ss._id in list_id_super sort ss.created_at 
        for op in ss.list_operation filter op.route == @data.route
        for r in op.list_result filter r.basedon == @data.basedon
        return r.value
                `, { data: data })

        return result._result;

    }


}

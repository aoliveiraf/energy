const db = require('../config/database')

module.exports = {
    getResult: async function (id_sample,property_name) {
        console.log("getResult");
        console.log(property_name);
        
        const result = await db.query(`
        for s in sample filter s._id == @id_sample
        RETURN s.list_result[@property_name]
                                    `,
            {id_sample: id_sample,property_name:property_name})

        let r = {}
        r[property_name] = result._result[0];
        return r;
    },
    updateResult:async function(id_sample, data) {

        await db.query(`for sample in sample filter sample._id==@id_sample 
        update sample with {"list_result":@data} in sample
        `,{id_sample:id_sample,data:data})
    }

}

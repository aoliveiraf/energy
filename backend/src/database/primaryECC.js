const db = require('../config/database')
const db_survey = require('../database/survey')

module.exports = {
    getPropertyValueByPropertyName: async function (id_sample,basedon,limit) {
        console.log("getPropertyValueByPropertyName");
                
        const result = await db.query(`
        let result = (for s in sampleECCItem filter s.id_sample == @id_sample and s.timestamp >= @limit.started_at and s.timestamp <= @limit.finished_at
            RETURN (
               FOR name IN attributes(s.list_property)
               filter name == @property_name
                 RETURN s.list_property[name]
           ))
           
           return flatten(result)
                            `,
            {id_sample: id_sample,property_name:basedon,limit:limit})

        return result._result;
    }

}

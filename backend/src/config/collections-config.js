const collectionConfig = [
    {name:"surveyModel", type: 1},
    {name:"sample", type: 1},
    {name:"sampleAppItem",type:1},
    {name:"sampleECCItem",type:1},
    {name:"app", type: 1},//ML app or collect property app
    {name:"ECC", type: 1}, // Energy consumption component
    {name:"ECCClass", type: 1},
    {name:"ECCSystem", type: 1},
    {name:"dataset", type: 1},
    {name:"datasetItem", type: 1}

];

module.exports = collectionConfig;
require('dotenv').config();
const express = require('express');
const index = require('./src/routes/index');
const path = require('path');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');

const util = require('./src/routes/util')

const alexNet = require('./src/routes/app/alexNet');
const collector = require('./src/routes/app/collector');

const canonicalOperationSecondaryECC = require('./src/routes/operation/canonical/secondaryECC')
const canonicalOperationSecondaryGen = require('./src/routes/operation/canonical/secondaryGen')
const canonicalOperationPrimaryECC = require('./src/routes/operation/canonical/primaryECC')
const canonicalOperationPrimaryGen = require('./src/routes/operation/canonical/primaryGen')

const update_last_samples = require('./src/routes/operation/canonical/update_last_samples')

const surveyModel = require('./src/routes/surveyModel')
const survey = require('./src/routes/survey')
const ecc = require('./src/routes/ecc')
const _result = require('./src/routes/operation/canonical/result')
const dataChart = require('./src/routes/dataChart')


const app = express();

app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(morgan('dev'));


app.use('/',index)
app.use('/alexNet',alexNet);
app.use('/nvidiaGPUCollector',collector);

app.use('/ecc',ecc);
app.use('/canonicalOperation/secondaryECC',canonicalOperationSecondaryECC);
app.use('/canonicalOperation/secondaryGen',canonicalOperationSecondaryGen);
app.use('/canonicalOperation/primaryECC',canonicalOperationPrimaryECC);
app.use('/canonicalOperation/primaryGen',canonicalOperationPrimaryGen);
app.use('/canonicalOperation/result',_result);

app.use('/updateLastSample',update_last_samples)

app.use('/dataChart',dataChart);



app.use('/survey',survey);
app.use('/surveyModel',surveyModel);
app.use('/util',util);

app.listen(3000,()=>{
    console.log('listening...',3000);
})


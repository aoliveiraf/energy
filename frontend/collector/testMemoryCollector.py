from collector import MemoryGPUCollector

col = MemoryGPUCollector(
    name="Test",
    urlSourceCode="https://gitlab.com/aoliveiraf/energy-collector.git",
    urlSample="http://localhost:3000/sample/csv",
    numSampling=2,listParam=["gpu_name","gpu_bus_id","power.draw"])

col.run()
# to run the container makes this:
# docker run -d -it -v $PWD:/tmp -v /var/run/docker.sock:/var/run/docker.sock -w /tmp manager python container_manager.py 225511 http://192.168.56.1:3000
# docker run -d -it nvidia/cuda nvidia-smi --loop-ms=100 --query-gpu=power.draw,pcie.link.gen.current,pcie.link.width.current,memory.used,memory.free,utilization.gpu,clocks.current.g raphics,clocks.current.sm,clocks.current.memory --format=csv  
# argv[1] = container app properties collector
# argv[2] = container app machine learning

import docker
import sys
import subprocess
import threading
import time
from requests import get,post,put,delete
import json
import numpy as np


def get_app_detail(key_survey_model,url_port_server):
   
   route = url_port_server+"/survey/"+key_survey_model

   print(route)
   result = get(route)

   return result.json()

def new_survey_instance(url,key_survey_model):

    data = {
        "key_surveyModel":key_survey_model,
        "created_at":int(time.time()*1000.0)
    }

    r= post(url+"/survey/surveyInstance", data = json.dumps(data),headers={"Content-Type": "application/json"})
    
    return r.json()['_key']

def new_sample(url,key_survey_instance):

    data ={
        "key_survey_instance":key_survey_instance,
        "created_at":int(time.time()*1000.0)
    }
    r= post(url+"/survey/sample", data = json.dumps(data),headers={"Content-Type": "application/json"})
    
    return r.json()['_key']

def test_pattern(list_pattern,row):
    for pattern in list_pattern:
        if pattern['id'] in row:
            return True
    
    return False


def read_and_send_log_gen(container_name,server_port_route,list_parameter,batch_size,key_survey_model,key_sample):


    client = docker.from_env()
    container = client.containers.get(container_name)  
    logs = container.logs(stream=True,timestamps=True)

    batch=[]
    start = False
   
    if "start_term" not in list_parameter:
        start = True

    for i,line in enumerate(logs):

        row = line.decode("utf-8")

        if not start: 
            if "start_term" in list_parameter and list_parameter['start_term'] in row:
                start = True 
        else: 
            if "finish_term" in list_parameter and list_parameter['finish_term'] in row:
                break

            batch.append(row)

        if batch_size == len(batch):
            print(batch) 
          #  send_data(route=server_port_route,key_survey_model=key_survey_model,key_sample=key_sample, batch=batch)
            batch=[]

    if len(batch)>0: 
        print(batch)
       # send_data(route=server_port_route,key_survey_model=key_survey_model,key_sample=key_sample, batch=batch)
        batch=[]


def read_and_send_log_char(container_name,server_port_route,list_parameter,batch_size,key_characterization):


    client = docker.from_env()
    container = client.containers.get(container_name)  
    logs = container.logs(stream=True,timestamps=True)

    batch=[]
    start = False
   
    if "start_term" not in list_parameter:
        start = True

    for i,line in enumerate(logs):

        row = line.decode("utf-8")

        if not start: 
            if "start_term" in list_parameter and list_parameter['start_term'] in row:
                start = True 
        else: 
            if "finish_term" in list_parameter and list_parameter['finish_term'] in row:
                break

            batch.append(row)

        if batch_size == len(batch): 
            dataToSend= {
                "key_characterization":key_characterization,
                "batch":batch
            }

            post(route, data = json.dumps(dataToSend),headers={"Content-Type": "application/json"})
            batch=[]

    if len(batch)>0: 
        dataToSend= {
            "key_characterization":key_characterization,
            "batch":batch
        }

        post(route, data = json.dumps(dataToSend),headers={"Content-Type": "application/json"})
        batch=[]


def read_and_send_log_coll(container_name,server_port_route,batch_size,key_survey_model,key_sample,list_device):

    client = docker.from_env()
    container = client.containers.get(container_name)  
    logs = container.logs(stream=True,timestamps=True)

    batch=[]
    batch_size = self.plan["batch_size"]
    for i,line in enumerate(logs):
        row = line.decode("utf-8")

        if test_pattern(list_device,row):
            batch.append(row)

        if batch_size == len(batch): 
            send_data(route=server_port_route,key_survey_model=key_survey_model,key_sample=key_sample, batch=batch)
            batch=[]

    if len(batch)> 0: 
        send_data(route=server_port_route,key_survey_model=key_survey_model,key_sample=key_sample, batch=batch)
        batch=[]


def turn_phase_from_coll(client,container_name,server_port,key_sample):
        
    container = client.containers.get(container_name)

    logs = container.logs(stream=True,timestamps=True)
    row = ""
    for i,line in enumerate(logs):
        if i ==0:
            row = line.decode('utf-8')
        break
    data = {
        "key_sample":key_sample,
        "line":row
    }
    put(server_port + "/survey/sample/turnIntoPhase1", data = json.dumps(data),headers={"Content-Type": "application/json"})

def turn_phase_from_gen(client,container_name,server_port,key_sample,list_parameter):
    container = client.containers.get(container_name)

    logs = container.logs(stream=True,timestamps=True)
    start = False
    turn_sample_into_phase_3 = False
    turn_sample_into_phase_4 = False

    if "start_term" not in list_parameter:
        start = True

    row_before = ""
    row =""
    for i,line in enumerate(logs):
        row = line.decode("utf-8")
        if i == 0:
            turn_sample_into_phase(key_sample,row, server_port + "/survey/sample/turnIntoPhase2")

        if not start and "start_term" in list_parameter and list_parameter['start_term'] in row:
            start = True

        elif start: 
            if not turn_sample_into_phase_3:
                turn_sample_into_phase_3 = True
                turn_sample_into_phase(key_sample,row,server_port + "/survey/sample/turnIntoPhase3")

            elif "finish_term" in list_parameter and list_parameter['finish_term'] in row:
                if not turn_sample_into_phase_4:
                    turn_sample_into_phase_4 = True
                    turn_sample_into_phase(key_sample,row_before, server_port + "/survey/sample/turnIntoPhase4")
                break

        row_before = row

    if not turn_sample_into_phase_4:
        turn_sample_into_phase(key_sample,row_before, server_port + "/survey/sample/turnIntoPhase4")


def send_data(route,key_survey_model,key_sample,batch):

   dataToSend= {
      "key_sample":key_sample,
      "key_surveyModel":key_survey_model,
      "batch":batch
   }

   post(route, data = json.dumps(dataToSend),headers={"Content-Type": "application/json"})
   time.sleep(0.5)


def finish_sample(key_sample, url_port_server):
    dataToSend = {
        "key_sample":key_sample,
        "finished_at":int(time.time()*1000.0)
    }
    put(url_port_server+"/survey/sample/finish", data = json.dumps(dataToSend),headers={"Content-Type": "application/json"})
    time.sleep(0.1)
    print("finish sample.")

def finish_survey_instance(key_survey_instance, url_port_server):

    dataToSend = {
        "key_survey_instance":key_survey_instance,
        "finished_at":int(time.time()*1000.0)
    }

    put(url_port_server+"/survey/surveyInstance/finish", data = json.dumps(dataToSend),headers={"Content-Type": "application/json"})
    time.sleep(0.1)
    print("finish survey instance.")

def argument_for_container_run(container):
    list_argument = {}
        
    list_argument["image"] = container["image_name"]
    list_argument["name"] = container["name"]
    list_argument["detach"] = True

    if "runtime" in container:
        list_argument["runtime"] = container["runtime"]

    if "list_volume" in container:
        list_volume = container["list_volume"]

        volumes = {}
        for v in list_volume:
            volumes[v["path"]]={"bind":v["bind"],"mode":v["mode"]}

        list_argument["volumes"] = volumes
        
    if "working_dir" in container:
        list_argument["working_dir"] = container["working_dir"]

    if "command" in container:
        list_argument["command"] = container["command"]

    return list_argument


def stop_remove_container(client, container_name_ecc,container_name_app):
    try:
        container = client.containers.get(container_name_app)  
        container.stop()
        container.remove()
    except docker.errors.NotFound as identifier:
        pass

    try:
        container = client.containers.get(container_name_ecc)  
        container.stop()
        container.remove()
    except docker.errors.NotFound as identifier:
        pass

def start_phase_1():
    pass

def  turn_sample_into_phase(key_sample,row, server_port_route):
    print("turn_sample_into_phase")
    print(server_port_route)

    data = {
        "key_sample":key_sample,
        "line":row
    }
    put(server_port_route, data = json.dumps(data),headers={"Content-Type": "application/json"})

def turn_ecc_into_phase_0_true(server_port,list_device):

    data = {
        "list_device":list_device,
        "time_ms":int(time.time()*1000.0),
        "this":True
    }

    put(url_port_server+"/ecc/turnECCIntoPhase0", data = json.dumps(data),headers={"Content-Type": "application/json"})

def turn_ecc_into_phase_0_false(server_port,list_device):

    data = {
        "list_device":list_device,
        "time_ms":int(time.time()*1000.0),
        "this":False
    }

    put(url_port_server+"/ecc/turnECCIntoPhase0", data = json.dumps(data),headers={"Content-Type": "application/json"})


def operationSampleColl(plan,list_operation,key_sample,url_port_server):


    for index in plan["list_coll_operation_for_each_sample"]:

        operation = list_operation[index]
        print(operation["route"])
        data = {
            "key_sample":key_sample,
            "data_send":operation["data_send"]
        }
        put(url_port_server+operation["route"], data = json.dumps(data),headers={"Content-Type": "application/json"})

def operationSampleGen(plan,list_operation,key_sample,url_port_server):

    for index in plan["list_gen_operation_for_each_sample"]:

        operation = list_operation[index]

        print(operation["route"])
        data = {
            "key_sample":key_sample,
            "data_send":operation["data_send"]
        }
        put(url_port_server+operation["route"], data = json.dumps(data),headers={"Content-Type": "application/json"})

def operationSurveyColl(plan,list_operation,key_survey_instance,url_port_server):

    for index in plan["list_coll_operation_for_each_survey"]:

        operation = list_operation[index]

        print(operation["route"])
        data = {
            "key_survey_instance":key_survey_instance,
            "data_send":operation["data_send"]
        }
        put(url_port_server+operation["route"], data = json.dumps(data),headers={"Content-Type": "application/json"})

def operationSurveyGen(plan,list_operation,key_survey_instance,url_port_server):

    for index in plan["list_gen_operation_for_each_survey"]:

        operation = list_operation[index]

        print(operation["route"])
        data = {
            "key_survey_instance":key_survey_instance,
            "data_send":operation["data_send"]
        }
        put(url_port_server+operation["route"], data = json.dumps(data),headers={"Content-Type": "application/json"})

def  update_survey_model_survey_instance(key_survey_model,key_survey_instance,index_plan):
    print("UNDER DEVELOPMENT: update_survey_model_survey_instance")

def  update_survey_model_index_current_sample_coll_start(key_survey_model,index_plan,index_sample):
    print("UNDER DEVELOPMENT: update_survey_model_index_current_sample_gen")

def  update_survey_model_index_current_sample_gen_start(key_survey_model,index_plan,index_sample):
    print("UNDER DEVELOPMENT: update_survey_model_index_current_sample_gen")

def update_survey_model_index_current_sample_gen_finish(key_survey_model,index_plan,index_sample):
    print("UNDER DEVELOPMENT: update_survey_model_index_current_sample_gen_finish")

def update_survey_model_index_current_sample_coll_finish(key_survey_model,index_plan,index_sample):
    print("UNDER DEVELOPMENT: update_survey_model_index_current_sample_coll_finish")

def getCharacterization(url_port_server,key_survey_model):

   route = url_port_server+"/caracterization/"+key_survey_model

   result = get(route)

   return result.json()

def newCharacterization(url_port_server,survey_model):

    route = url_port_server+surveyModel["generator"]["route"]
    
    key_app = surveyModel["collector"]["ref_app"]["ref"]

    data = {
        "key_app":key_app
    }

    r= post(url_port_server+"/characterization", data = json.dumps(data),headers={"Content-Type": "application/json"})

    return r.json()['_key']


def newCharacterizationItem(url_port_server,client,survey_model):

    list_parameter = surveyModel["collector"]["ref_app"]["protocol_detail"]["list_argument"]
    route = url_port_server+surveyModel["generator"]["route"]
    key_survey_model = survey_model._key
    
    list_argument = argument_for_container_run(surveyModel["generator"][index_gen]["container"])

    container_name_app = list_argument_gen['name']
    
    client.containers.run(**list_argument)
    
    waitingForContainerFinish(client,container_name_app)
    
    read_and_send_log_char(container_name_app,route,list_parameter,1000,key_characterization)



def initProcess(survey_model):

    characterization = getCharacterization(url_port_server,survey_model._key)

    if characterization == None:
        characterization = newCharacterization(survey_model)

        newCharacterization(client,survey_model)


def prepareSetupECC(key_survey_model,url_port_server):
    pass

def setupECC():
    indexSetup=indexSetup+1    

def run_container_generator():

    if("generator" in plan):
        
        generator = plan["generator"]
        container = generator["generator"]

        list_argument = argument_for_container_run(container)

        client.containers.run(**list_argument)
        run_generator = True
    


class Manager:

    current_container_index = -1
    survey_model = None
    list_key_super_sample_by_level = []
    plan = None
    time_factor = None
    running_coll=False
    running_coll_name=None
    running_gen = False
    running_gen_name=None
    logs_coll = []
    logs_gen = []
    PRIMARY_LEVEL = 0
    current_container_detail = None
    current_stop_timer=None
    current_start_timer=None
    stop_row = ""
    num_level = 3
    

    def  __init__(self,server_port=None,key_survey_model=None):

        self.client = docker.from_env()
        self.server_port = server_port
        self.init_survey_model(key_survey_model)
        self.init_level_sample()
        

    def init_level_sample(self):

        for _ in range(self.num_level):
            self.list_key_super_sample_by_level.append("")

    def stop_coll(self):
        print("stop coll")
        if self.running_coll:
            container = self.client.containers.get(self.running_coll_name)  
            container.stop()
            self.current_stop_time=int(time.time()*1000.0)
            self.running_coll = False


    def stop_gen(self):
        print("stop gen")
        if self.running_gen:
            container = self.client.containers.get(self.running_gen_name)  
            container.stop()
            self.current_stop_time=int(time.time()*1000.0)
            self.running_gen = False

    def stop_remove_container(self,container_name):
        print("stop_remove_container")
        try:
            container = self.client.containers.get(container_name)  
            container.stop()
            container.remove()
        except docker.errors.NotFound as identifier:
            pass

    def read_and_send_log_to_user_property_coll(self):

        print("read_and_send_log_to_user_property_coll")
        route = self.server_port + self.plan["collector"]["route"]
        for i,line in enumerate(self.logs_coll):
            if self.plan["collector"]['log']['max_line_read'] > -1 and i >= self.plan["collector"]['log']['max_line_read']:
                break

            row = line.decode("utf-8")

            if self.test_device(row):
                batch.append(row)

            if batch_size == len(batch):
                self.send_data(route=route, batch=batch,list_property=self.current_container_detail["list_property"]) 
                batch=[]

        if len(batch)> 0: 
            self.send_data(route=route, batch=batch,list_property=self.current_container_detail["list_property"]) 
            batch=[]


    def send_log(self,logs,route):

        print("send_log")
        batch=[]
        batch_size = self.plan["batch_size"]
        url = self.server_port + route

        for i,line in enumerate(logs):

            row = line.decode("utf-8")
            if self.stop_row == row:
                break

            batch.append(row)

            if batch_size == len(batch):
                self.send_data(route=url, batch=batch,list_property=self.current_container_detail["list_property"]) 
                batch=[]

        if len(batch)> 0: 
            self.send_data(route=url, batch=batch,list_property=self.current_container_detail["list_property"]) 
            batch=[]


    def read_and_send_log_coll(self):

        print("read_and_send_log_coll")
        batch=[]
        batch_size = self.plan["batch_size"]
        route = self.server_port + self.plan["collector"]["route"]
        for i,line in enumerate(self.logs_coll):

            if self.plan['log']['max_line_read'] > -1 and i >= self.plan['log']['max_line_read']:
                break

            row = line.decode("utf-8")

            batch.append(row)

            if batch_size == len(batch):
                self.send_data(route=route, batch=batch,list_property=self.current_container_detail["list_property"]) 
                batch=[]

        if len(batch)> 0: 
            self.send_data(route=route, batch=batch,list_property=self.current_container_detail["list_property"]) 
            batch=[]

    def waiting_log_size(self,logs):

        print("waiting_log_size")

        while True:
            for i,line in enumerate(logs):
                if i < self.plan["stop"]["line_read"]:
                    return True
            
                time.sleep(1)


    def send_data(self,route,list_property,batch):

        dataToSend= {
            "key_sample":self.list_key_super_sample_by_level[self.PRIMARY_LEVEL],
            "key_surveyModel":self.survey_model["_key"],
            "batch":batch,
            "list_property":list_property
        }
        
        post(route, data = json.dumps(dataToSend),headers={"Content-Type": "application/json"})
#        time.sleep(0.5)

    def test_device(self,row):
        for device in self.plan["collector"]["list_device"]:
            if device in row:
                return True
        
        return False

    def update_start_stop_current_sample_coll(self):
        dataToSend = {

            "key_sample":self.list_key_super_sample_by_level[0],
            "started_at":self.current_start_time,
            "stopped_at":self.current_stop_time
        }
        
        put(self.server_port+"/survey/sample/setStartStopTime",data = json.dumps(dataToSend),headers={"Content-Type": "application/json"})
    
    def process(self):
        print("process")

        self.preparePlan()
        self.num_level = int(self.plan["num_level"])

        list_container = self.plan["collector"]["list_container"]
        list_frequency = self.plan["collector"]["list_frequency"]
        list_operation = self.plan["collector"]["list_operation"]
        
        if self.num_level == 3:
            print("create 3-level Sample.")
            self.new_sample(level_sample=3)

        for k in range(len(list_container)):

            self.new_sample(level_sample=2)

            print("epoch: " + str(k) )
            for i in range(self.plan["repeat"]):

                print("primary sample: " + str(i) )
                self.current_container_detail = list_container[k]

                read_frequency_ms =0
                if "read_frequency_ms" in self.current_container_detail:
                    read_frequency_ms = self.current_container_detail["read_frequency_ms"]

                self.new_sample(level_sample=1,read_frequency_ms=read_frequency_ms)

                if "collector" in self.plan:
                    self.stop_remove_container(list_container[k]["name"])
                
                if "generator" in self.plan:  
                    self.stop_remove_container(self.plan["generator"]["container"]["name"])

                if "collector" in self.plan:
                    self.run_container_coll(list_container[k])

                if "generator" in self.plan:    
                    self.run_container_gen(self.plan["generator"]["container"])

                if "collector" in self.plan:                        
                    self.phase_identification_coll(self.list_key_super_sample_by_level[0])

                if "generator" in self.plan:                        
                    self.phase_identification_gen(self.list_key_super_sample_by_level[0],self.plan["generator"]["p3"])
                
                if "collector" in self.plan:
                    self.stop_coll()

                if "generator" in self.plan:
                    self.stop_gen()

                if "collector" in self.plan and self.plan["collector"]["send_data"]:                        
                    self.send_log(self.logs_coll,self.plan["collector"]["route"])

                if "generator" in self.plan and self.plan["generator"]["send_data"]:                        
                    self.send_log(self.logs_gen,self.plan["generator"]["route"])

#                self.update_start_stop_current_sample_coll()

                if self.run_operation(list_operation["primary"],self.list_key_super_sample_by_level[0]) == True:
                    return True

                print("sleep for: " + str(self.plan["sleep"]))
                time.sleep(self.plan["sleep"])

            if self.run_operation(list_operation["secondary"],self.list_key_super_sample_by_level[1]) == True:
                return True

            print("sleep for: " + str(self.plan["sleep"]))
            time.sleep(self.plan["sleep"])

        if self.num_level == 3:
            if self.run_operation(list_operation["tertiary"],self.list_key_super_sample_by_level[2]) == True:
                return True

        return True

    def run_container_gen(self,container_detail,timestamps=True):
        print("run_container_gen")
        self.waitting_stop_gen = False

        list_argument = self.set_container_detail(container_detail)
        container_instance = self.client.containers.run(**list_argument)
        self.logs_gen = container_instance.logs(stream=True,timestamps=timestamps)
        self.running_gen_name = container_detail["name"]
        self.running_gen=True
        print("container gen is running.")


    def run_operation(self,list_operation,key_sample):
        print("run_operation")
        abort = False
        print(key_sample)

        for operation in list_operation:
            data = {
                "key_sample":key_sample,
                "data_send":operation["data_send"]
            }
            route = operation["route"]
            print(route)
            r = put(self.server_port+route, data = json.dumps(data),headers={"Content-Type": "application/json"})
            print(r.json())
            if "abort" in r.json():
                abort = r.json()["abort"]
            
        return abort

    def wait_absolute(self):
        print("wait_absolute")

        time.sleep(self.plan["stop_early"]["first_wait"])

    def wait_relative(self,frequency):
        print("stop_relative")

        factor=self.plan["stop_early"]["first_wait"]*frequency
        
        time.sleep(factor)
        
    def run_container_to_sending_user_property_coll(self):
        print("run_container_for_user_property_coll")

        list_container_user_property = plan["collector"]["list_container_user_property"]

        for container_detail in list_container_user_property:
            self.run_container_coll(container_detail,timestamps= False)
            self.waiting_coll()

#doin deprecated candidate
    def run_container_coll(self,container_detail,timestamps=True):
        print("run_container_coll")
        self.waitting_stop_coll = False

#        self.stop_remove_container(container_detail["name"])

        list_argument = self.set_container_detail(container_detail)
        self.current_start_time=int(time.time()*1000.0)

        container_instance = self.client.containers.run(**list_argument)
        self.logs_coll = container_instance.logs(stream=True,timestamps=timestamps)
        self.running_coll_name = container_detail["name"]
        self.running_coll=True

        print("container coll is running")

    def waiting_early(self):
        print("waiting early...")

        if self.plan["stop_early"]["relative"]==False:
            self.wait_absolute()
        else :
            self.wait_relative()
        if self.plan["stop_early"]["log_from"]=="gen":

            self.waiting_log_size(self.logs_gen)   

        else:
            self.waiting_log_size(self.logs_coll)

        self.stop_coll()
        self.stop_gen()

               

    def wainting_gen_finish(self,key_sample):
        print("wainting_gen_finish")
        container = self.client.containers.get(self.running_gen_name)
        finish = False
        while finish:
            if container.status == 'exited':
                finish = True
            else:
                time.sleep(1)

        self.stop_coll()

    def preparePlan(self):
        print("preparePlan")
        route = self.server_port+"/surveyModel/prepareForRun/"+self.survey_model['_key']

        r = put(route,data = json.dumps({}),headers={"Content-Type": "application/json"}) 

        self.plan = r.json()
            
    def new_sample(self,level_sample=0,read_frequency_ms=0):
        print("new_sample")
        route = self.server_port+"/survey/sample"
        data ={
            "key_survey_model":self.survey_model['_key'],
            "created_at":int(time.time()*1000.0),
            "read_frequency_ms":read_frequency_ms,
            "level":level_sample
        }
        if  level_sample < self.num_level:
            data["key_super"] = self.list_key_super_sample_by_level[level_sample]

        r= post(route, data = json.dumps(data),headers={"Content-Type": "application/json"})
    
        self.list_key_super_sample_by_level[level_sample -1] = r.json()['_key']

        print(self.list_key_super_sample_by_level)

    def init_survey_model(self,key_survey_model):
        print("init_survey_model")
        route = self.server_port+"/survey/"+key_survey_model

        result = get(route)
        
        self.survey_model= result.json()

    def set_container_detail(self,container):
        print("set_container_detail")
        list_argument = {}
            
        list_argument["image"] = container["image_name"]
        list_argument["name"] = container["name"]
        list_argument["detach"] = True

        if "environment" in container:
            list_argument["environment"]=container["environment"]

        if "runtime" in container:
            list_argument["runtime"] = container["runtime"]

        if "list_volume" in container:
            list_volume = container["list_volume"]

            volumes = {}
            for v in list_volume:
                volumes[v["path"]]={"bind":v["bind"],"mode":v["mode"]}

            list_argument["volumes"] = volumes
            
        if "working_dir" in container:
            list_argument["working_dir"] = container["working_dir"]

        if "command" in container:
            list_argument["command"] = container["command"]

        return list_argument

    def deleteAllSample(self):
        print("deleteAllSample")
        route = self.server_port+"/surveyModel/"+self.survey_model["_key"]

        result = delete(route)

    def turn_phase_from_coll(self,key_sample):
            
        row = ""
        for i,line in enumerate(self.logs_coll):
            if i ==0:
                row = line.decode('utf-8')
            break
        data = {
            "key_sample":key_sample,
            "line":row
        }
        put(self.server_port + "/survey/sample/turnIntoPhase1", data = json.dumps(data),headers={"Content-Type": "application/json"})

    def turn_phase_from_gen(self,key_sample,list_parameter):

        start = False
        turn_sample_into_phase_3 = False
        turn_sample_into_phase_4 = False

        row_before = ""
        row =""
        for i,line in enumerate(self.logs_gen):
            row = line.decode("utf-8")
            if i == 0:
                turn_sample_into_phase(key_sample,row, self.server_port + "/survey/sample/turnIntoPhase2")

            if not start and "start_term" in list_parameter and list_parameter['start_term'] in row:
                start = True

            elif start: 
                if not turn_sample_into_phase_3:
                    turn_sample_into_phase_3 = True
                    turn_sample_into_phase(key_sample,row,self.server_port + "/survey/sample/turnIntoPhase3")

                elif "finish_term" in list_parameter and list_parameter['finish_term'] in row:
                    if not turn_sample_into_phase_4:
                        turn_sample_into_phase_4 = True
                        turn_sample_into_phase(key_sample,row_before, self.server_port + "/survey/sample/turnIntoPhase4")
                    break

            row_before = row

        if not turn_sample_into_phase_4:
            turn_sample_into_phase(key_sample,row_before, self.server_port + "/survey/sample/turnIntoPhase4")

    def phase_identification_gen(self,key_sample,list_parameter):
        print("phase_identification_gen")
        almost_phase_3 = False
        turn_sample_into_phase_3 = False
        turn_sample_into_phase_4 = False
        i_phase = 0
        phase = "P1"

        row_before = ""
        row =""
        for i,line in enumerate(self.logs_gen):

            row = line.decode("utf-8")
            
            if i == 0:
                turn_sample_into_phase(key_sample,row, self.server_port + "/survey/sample/turnIntoPhase2")
                phase="P2"

            if not almost_phase_3 and list_parameter['start_term'] in row:
                almost_phase_3 = True

            elif almost_phase_3: 
                if not turn_sample_into_phase_3:
                    turn_sample_into_phase_3 = True
                    turn_sample_into_phase(key_sample,row,self.server_port + "/survey/sample/turnIntoPhase3")
                    i_phase =0                    
                    phase ="P3"

                elif list_parameter['finish_term'] in row:
                    if not turn_sample_into_phase_4:
                        break

            row_before = row

            if self.stop_read_log(phase,i_phase):
                break

            i_phase+=1

        turn_sample_into_phase(key_sample,row_before, self.server_port + "/survey/sample/turnIntoPhase4")

    def phase_identification_coll(self,key_sample):
        print("phase_identification_coll")
        
        row = ""

        if "generator" in self.plan:

            for i,line in enumerate(self.logs_coll):
                
                row = line.decode('utf-8')
                if i ==0:
                    break

            turn_sample_into_phase(key_sample,row, self.server_port + "/survey/sample/turnIntoPhase1")

        else:
            for i,line in enumerate(self.logs_coll):

                row = line.decode('utf-8')
                if i ==0:
                    turn_sample_into_phase(key_sample,row, self.server_port + "/survey/sample/turnIntoPhase1")

                if self.stop_read_log("P1",i+1):
                    break
            
            turn_sample_into_phase(key_sample,row, self.server_port + "/survey/sample/turnIntoPhase4")
            

    def stop_read_log(self,phase,num):

        if self.plan["stop"]["phase"].upper() == phase.upper() and int(self.plan["stop"]["line_read"]) > 0 and int(self.plan["stop"]["line_read"]) <= num:
            return True
        elif phase.upper() > self.plan["stop"]["phase"].upper(): 
            return True

        return False 

container_plan=None

key_survey_model=sys.argv[1] # The saved survey has the two routes app_gen and app_coll. At the suervey there is all need properties 
url_port_server = sys.argv[2] # the routes are appended with this url to represents the full path.



#client = docker.from_env()

#client.containers.run(image= 'python', name= 'collector', detach= True, volumes= {'/c/Users/aoliv/projects/phd-project/energy/frontend/manager': {'bind': '/tmp', 'mode': 'rw'}}, working_dir= '/tmp', command= ['python', 'printout_coll.py','fgdg'])

manager = Manager(url_port_server,key_survey_model)

manager.deleteAllSample()

manager.process()

'''

print("get survey...")
surveyModel = get_app_detail(key_survey_model,url_port_server)

run_plan = surveyModel["run_plan"]

initProcess(surveyModel):


for index_plan,plan in enumerate(run_plan):
    key_survey_instance = new_survey_instance(url_port_server,key_survey_model)
    update_survey_model_survey_instance(key_survey_model,key_survey_instance,index_plan)
    
    print("survey plan: " + key_survey_model)
    print("survey instance: " + key_survey_instance)
    
    index_coll = plan["coll"]["index"]
    index_gen = plan["gen"]["index"]

    for repeat in range(plan["num_sample"]):

        key_sample = new_sample(url_port_server,key_survey_instance)

        print("sample: " + key_sample  + " " + str(repeat))

        container_name_ecc = surveyModel["collector"]["container"]["name"]
        container_name_app = surveyModel["generator"]["container"]["name"]

        list_argument_coll = argument_for_container_run(surveyModel["collector"]["container"])
        list_argument_gen = argument_for_container_run(surveyModel["generator"]["container"])

        stop_remove_container(client, container_name_ecc,container_name_app)    

        container_ecc =""
        if plan['coll']['run_container']:
            print('Coll is running...')
            update_survey_model_index_current_sample_coll_start(key_survey_model,index_plan,repeat)
            client.containers.run(**list_argument_coll)
        if plan['gen']['run_container']:
            print('Gen is running...')
            update_survey_model_index_current_sample_gen_start(key_survey_model,index_plan,repeat)
            client.containers.run(**list_argument_gen)

        t0 = int(time.time()*1000.0)
        print(t0)
        print('wating for finish.')

        waitingForContainerFinish(client,list_argument_gen['name'])

        t1 = int(time.time()*1000.0)
        print(t1-t0)
        print('finishing Gen.')
        print('Finishing Coll.')
        container_ecc = client.containers.get(list_argument_coll['name'])
        container_ecc.stop()

        list_parameter = surveyModel["collector"]["ref_app"]["protocol_detail"]["list_argument"]

        print('turn ecc into phase 0 false')
        turn_ecc_into_phase_0_false(url_port_server, surveyModel["collector"]['list_device'])
        print('Turn into phase 1')
        turn_phase_from_coll(client,container_name_ecc,url_port_server,key_sample)

        if plan["coll"]["send_data"]==True:

            route = url_port_server + surveyModel["collector"]["route"]
            print('Sendding data from coll.')
            read_and_send_log_coll(container_name_ecc,url_port_server + surveyModel["collector"]["route"],plan["coll"]["batch_size"],key_survey_model,key_sample,surveyModel["collector"]['list_device'])
            update_survey_model_index_current_sample_coll_finish(key_survey_model,index_plan,repeat)

            operationSampleColl(plan,surveyModel["list_operation"],key_sample,url_port_server)

        list_parameter = surveyModel["generator"]["ref_app"]["protocol_detail"]["list_argument"]
        print('Turn into phase 2,3,4')
        turn_phase_from_gen(client,container_name_app,url_port_server,key_sample,list_parameter)

        if plan["gen"]["send_data"]==True:

            route = url_port_server + surveyModel["generator"]["route"]
            print('Sendding data from gen.')
            read_and_send_log_gen(container_name_app,url_port_server+surveyModel["generator"]["route"],list_parameter,plan["gen"]["batch_size"],key_survey_model,key_sample,index_gen)
    
            update_survey_model_index_current_sample_gen_finish(key_survey_model,index_plan,repeat)

            operationSampleGen(plan,surveyModel["list_operation"],key_sample,url_port_server)

        print("turn ecc into phas 0 true")
        turn_ecc_into_phase_0_true(url_port_server, surveyModel["collector"]['list_device'])
        finish_sample(key_sample,url_port_server)
    finish_survey_instance(key_survey_instance,url_port_server)

    operationSurveyColl(plan,surveyModel["list_operation"],key_survey_instance,url_port_server)
        
    operationSurveyGen(plan,surveyModel["list_operation"],key_survey_instance,url_port_server)

    stop_remove_container(client, container_name_ecc,container_name_app)    

   '''
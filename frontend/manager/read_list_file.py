# On this path /opt/ml-benchmarks/input/mlfull are all imagenet images.

path = sys.argv[2]

#path = '/content/sample_data'

files = [f for f in glob.glob(path + "**/*.csv", recursive=True)]

for f in files:
  file_stats = os.stat(f)

  #print(file_stats)
  print(f'File Size in Bytes is {file_stats.st_size}')
 # print(f'File Size in MegaBytes is {file_stats.st_size / (1024 * 1024)}')



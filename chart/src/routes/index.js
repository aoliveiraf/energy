const router = require('express').Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({ title: 'To read GPU Energy' });
});

module.exports = router;
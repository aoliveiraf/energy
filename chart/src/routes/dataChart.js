const router = require('express').Router();
const db_sample = require('../database/sample')

router.get('/freq_vs_std/:key_super', async (req, res) => {

    const data = {
        "key_super": req.params.key_super,
        "route": "/canonicalOperation/primaryECC/standardDeviation",
        "basedon": "power.draw"
    }

    const list_freq = await db_sample.getFrequency(data);
    const list_std = await db_sample.getValue(data);

    const result = [
        list_freq,
        list_std
    ]
    res.status(200).send(result);

})

router.get('/freq_vs_counter/:key_super', async (req, res) => {

    const data = {
        "key_super": req.params.key_super,
        "route": "/canonicalOperation/primaryECC/average",
        "basedon": "power.draw"

    }

    const list_freq = await db_sample.getFrequency(data);
    const list_av = await db_sample.getValue(data);

    const result = [
        list_freq,
        list_av
    ]
    res.status(200).send(result);
})


router.get('/standardDeviation/:key_super', async (req, res) => {

    const data = {
        "key_super": req.params.key_super,
        "route": "/canonicalOperation/primaryECC/standardDeviation",
        "basedon": "power.draw"
    }

    const list = await db_sample.getValue(data);

    res.status(200).send(list);
})

router.get('/:key_super/power/average', async (req, res) => {


    const data = {
        "key_super": req.params.key_super,
        "route": "/canonicalOperation/primaryECC/average",
        "basedon": "power.draw",
        "phase":"P3"
    }

    const list = await db_sample.getValue(data);

    res.status(200).send(list);
})


router.get('/sample/frequency/:key_sample', async (req, res) => {

    const data = {
        "key_super": req.params.key_sample
    }

    console.log(data);

    const list = await db_sample.getFrequency(data);

    console.log(list);

    res.status(200).send(list);
})

router.get('/surveyModel/:key_survey_model', async (req, res) => {

    const data = {
        "key_survey_model": req.params.key_survey_model
    }

    const list = await db_sample.getSurveyModel(data);

    res.status(200).send(list);
})

router.put('/levelValue', async (req, res) => {


    const list = await db_sample.getUpLevelValue(req.body);

    res.status(200).send(list);
})


router.put('/levelUpValue', async (req, res) => {


    const list = await db_sample.getUpLevelValue(req.body);

    res.status(200).send(list);
})
router.put('/valueThisLevelOrFrequency', async (req, res) => {


    const list = await db_sample.getValueThisLevelOrFrequency(req.body);

    res.status(200).send(list);
})


module.exports = router;
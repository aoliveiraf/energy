const db = require('../config/database')

module.exports = {

    getUpLevelValue: async function (data) {

        data.id_survey_model = `surveyModel/${data.key_survey_model}`;

        const result = await db.query(`
            let list_id_super = (for ss in sample filter ss.id_survey_model == @data.id_survey_model and HAS(ss,"list_operation")
            for op in ss.list_operation filter op.route == @data.route
            for r in op.list_result filter r.basedon == @data.basedon
            for s in sample filter has(s,"id_super") and s.id_super == ss._id and s.level == @data.base_level
            return DISTINCT ss._id)
            
            for ss in sample filter ss._id in list_id_super sort ss.created_at 
            for op in ss.list_operation filter op.route == @data.route
            for r in op.list_result filter r.basedon == @data.basedon
            return r.value
                    `, { data: data })

        return result._result;

    },

    getValue: async function (data) {

        data.id_super = `sample/${data.key_super}`;

        const result = await db.query(`
        for s in sample filter  s.id_super == @data.id_super and HAS(s,"list_operation") sort s.read_frequency_ms 
            for op in s.list_operation filter op.route == @data.route
                for r in op.list_result filter r.basedon == @data.basedon and r.phase=="P3"
                return r.value
        `, { data: data })

        return result._result;

    },

    getValueThisLevelOrFrequency: async function (data) {

        data.id_survey_model = `surveyModel/${data.key_survey_model}`;

        const result = await db.query(`
        for s in sample filter s.id_survey_model == @data.id_survey_model 
        and 
        (not HAS(@data,"read_frequency_ms") or s.read_frequency_ms == @data.read_frequency_ms) 
        and (not HAS(@data,"level") or s.level == @data.level)
        sort s.read_frequency_ms 
        
        for op in s.list_operation filter op.route == @data.route
        for r in op.list_result filter r.basedon == @data.basedon and r.phase==@data.phase

        return r.value`, 
        { data: data })

        return result._result;

    },

    getFrequency: async function (data) {

        data.id_super = `sample/${data.key_super}`;

        const result = await db.query(`
                for s in sample filter s.id_super == @data.id_super and HAS(s,"list_operation") sort s.read_frequency_ms 
                return s.read_frequency_ms
                `,
            { data: data })

        return result._result;

    }
    ,

    getAllSampleSurvey: async function (data) {

        const result = await db.query(`
                for sm in surveyModel 
                for s in sample filter s.level == @data.level and s.id_survey_model == sm._id 
                return s`, { data: data })

        return result._result;

    },

    getSurveyModel: async function (data) {

        data.id = `surveyModel/${data.key_survey_model}`;

        const result = await db.query(`
                for sm in surveyModel filter  HAS(sm,"collector") AND  sm._id == @id   
                return sm`, { id: data.id })

        return result._result[0];

    }

}

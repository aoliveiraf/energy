
const server_port = "http://localhost:3001"
let element_id = 0;

async function get_data(route) {
  const url = server_port + route;
  const res = await fetch(url)
  const result = await res.json()
  return result
}

async function get_data_post(route, body) {
  const url = server_port + route;
  const res = await fetch(url,
    {
      headers: {
        "Content-Type": "application/json"
      },
      method: "PUT",
      body: JSON.stringify(body)
    }
  )
  const result = await res.json()
  return result
}

async function display_chart_power_std() {

  const surveyModel = await get_data("/dataChart/surveyModel/5980");

  const labels = surveyModel[0].run_plan.collector.list_frequency;

  const datasets = [
    {
      label: "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
      borderWidth: 2,
      borderColor: 'rgba(77,166,253,0.90)',
      backgroundColor: 'transparent',
      data: await get_data("/dataChart/standardDeviation/1272038")
    },
    {
      label: "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
      borderWidth: 2,
      borderColor: 'rgba(235, 70, 52,0.90)',
      backgroundColor: 'transparent',
      data: await get_data("/dataChart/standardDeviation/3812492")
    }

  ]

  create_data_line_chart({ yLabel: "Power Standard Desviation", xLabel: "Frequency", title: "Read Frequency vs Standard Deviation Power", labels: labels, datasets: datasets })

}

async function display_chart_power_av() {

  const surveyModel = await get_data("/dataChart/surveyModel/5980");

  const labels = surveyModel[0].run_plan.collector.list_frequency;

  const datasets = [
    {
      label: "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
      borderWidth: 2,
      borderColor: 'rgba(77,166,253,0.90)',
      backgroundColor: 'transparent',
      data: await get_data("/dataChart/1272038/power/average")
    },
    {
      label: "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
      borderWidth: 2,
      borderColor: 'rgba(235, 70, 52,0.90)',
      backgroundColor: 'transparent',
      data: await get_data("/dataChart/3812492/power/average")
    }
  ]

  create_data_line_chart({ yLabel: "Power Average", xLabel: "Frequency", title: "Read Frequency vs Power Average", labels: labels, datasets: datasets })

}


function create_data_line_chart(data) {

  const ctx = create_canvas("panel");

  const myChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: data.labels,
      datasets: data.datasets
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: data.title,
        fontSize: 20
      },
      scales: {
        responsive: true,
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true
        }
      }
    }
  });


}

function create_data_bar_chart(data) {

  const ctx = create_canvas("panel");

  const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: data.labels,
      datasets: data.datasets,
      type: data.type
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: data.title,
        fontSize: 20
      }
    }
  });
}

async function get_data_by_level(key_survey_model) {

  const data = {
    "key_survey_model": key_survey_model,
    "route": "/canonicalOperation/secondaryECC/average",
    "basedon": "power.draw",
    "base_level": 1
  }

  return await get_data_post("/dataChart/levelValue", data);
}

async function display_chart_power_av_of_freq(key_suvey_average, key_survey_base, title) {

  const surveyModel = await get_data("/dataChart/surveyModel/" + key_survey_base);

  const labels = surveyModel[0].run_plan.collector.list_frequency;

  const av = await get_data_post("/dataChart/levelValue",
    {
      "key_survey_model": key_suvey_average,
      "route": "/canonicalOperation/secondaryECC/average",
      "basedon": "power.draw",
      "base_level": 1
    })

  let list_av = []
  for (let i = 0; i < labels.length; i++) {
    list_av.push(av[0])
  }

  console.log(list_av);

  const datasets = [
    {
      label: "1,10,...,100 read frequency",
      borderWidth: 2,
      borderColor: 'rgba(77,166,253,0.90)',
      backgroundColor: 'transparent',
      data: await get_data_post("/dataChart/levelValue",
        {
          "key_survey_model": key_survey_base,
          "route": "/canonicalOperation/secondaryECC/average",
          "basedon": "power.draw",
          "base_level": 1
        })
    },
    {
      label: "101 read frequency",
      borderWidth: 2,
      borderColor: "rgba(235, 70, 52,0.90)",
      backgroundColor: 'transparent',
      type: "line",
      data: list_av
    }
  ]

  create_data_bar_chart({ yLabel: "Power Average", xLabel: "Frequency", title: title, labels: labels, datasets: datasets })

}

function create_canvas(base_element) {

  const container = document.getElementById(base_element);
  const canvas = document.createElement("canvas");
  container.appendChild(canvas);

  canvas.id = ++element_id;
  const link = document.createElement("a");
  container.appendChild(link);
  link.innerText = "Download";
  link.download = "ChartImage.png";
  link.href = "";
  link.class = "btn btn-primary float-right bg-flat-color-1";
  link.title = "Descargar Gráfico";

  link.id = ++element_id;

  link.addEventListener('click', () => {
    var url_base64jp = document.getElementById(canvas.id).toDataURL("image/png");
    link.href = url_base64jp;
  })
  const ctx = canvas.getContext('2d');

  return ctx;
}

async function display_chart(generic_data, datasets) {

  const ctx = create_canvas("panel");

  const myChart = new Chart(ctx, {
    type: generic_data.type,
    data: {
      labels: generic_data.labels,
      datasets: datasets,
      backgroudColor: generic_data.gackgoundColor,
      borderColor: generic_data.borderColor,
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: generic_data.title,
        fontSize: 20
      }
    }
  });
}

async function main() {
  /**
   * AlexNet  [1] + 1x[10, 20, ... 1000] 
   */
  display_chart(
    {
      "title": "Time (Millisecond) vs Energy (Joule) - AlexNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/5980")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond Time",
      "type": "line"
    },
    [
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "5980",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/energy"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "3812459",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/energy"
          })
      }
    ])

  display_chart(
    {
      "title": "Time (Millisecond) vs Power Average (Watt) - AlexNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/5980")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond",
      "type": "line"
    },
    [
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "5980",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/average"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "3812459",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/average"
          })
      }
    ])

  display_chart(
    {
      "title": "Time (Millisecond) vs Power Standard Deviation (Watt) - AlexNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/5980")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond",
      "type": "line"
    },
    [
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "5980",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/standardDeviation"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "3812459",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/standardDeviation"
          })
      }
    ])


  /**
* AlexNet [1] + 10x [10,20, ... 100)
*/

  display_chart(
    {
      "title": "Time (Millisecond) vs Power Average (Watt) - AlexNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/8922720")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond",
      "type": "line"
    },
    [
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/levelValue",
          {
            "key_survey_model": "8922720",
            "route": "/canonicalOperation/secondaryECC/average",
            "basedon": "power.draw",
            "base_level": 1
          })
      },
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/levelValue",
          {
            "key_survey_model": "20793470",
            "route": "/canonicalOperation/secondaryECC/average",
            "basedon": "power.draw",
            "base_level": 1
          })
      }
    ])

  display_chart(
    {
      "title": "Time (Millisecond) vs Power Standard Deviation (Watt) - AlexNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/8922720")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond",
      "type": "line"
    },
    [
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/levelValue",
          {
            "key_survey_model": "8922720",
            "route": "/canonicalOperation/secondaryECC/standardDeviation",
            "basedon": "power.draw",
            "base_level": 1
          })
      },
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/levelValue",
          {
            "key_survey_model": "20793470",
            "route": "/canonicalOperation/secondaryECC/standardDeviation",
            "basedon": "power.draw",
            "base_level": 1
          })
      }
    ])

  /**
* mobileNet  [1] + 1x[10, 20, ... 1000] 
*/


  display_chart(
    {
      "title": "Time (Millisecond) vs Power Average (Watt) - MobileNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/43511847")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond Time",
      "type": "line"
    },
    [
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "43511847",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/average"
          })
      }
    ])

  display_chart(
    {
      "title": "Time (Millisecond) vs Energy (Joule) - MobileNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/43511847")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond Time",
      "type": "line"
    },
    [
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "43511847",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/energy"
          })
      }
    ])

  display_chart(
    {
      "title": "Time (Millisecond) vs Power Average (Watt) - MobileNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/34675581")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond Time",
      "type": "line"
    },
    [
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "33471021",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/average"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "34675581",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/average"
          })
      }
    ])

  display_chart(
    {
      "title": "Time (Millisecond) vs Power Standard Deviation (Watt) - MobileNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/34675581")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond Time",
      "type": "line"
    },
    [
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "33471021",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/standardDeviation"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "34675581",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/standardDeviation"
          })
      }
    ])

  display_chart(
    {
      "title": "Time (Millisecond) vs Energy (Joule) - MobileNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/34675581")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond Time",
      "type": "line"
    },
    [
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "33471021",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/energy"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "34675581",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/energy"
          })
      }
    ])


  /**
   * Alexnet and mobilenet
   * 
   */


  display_chart(
    {
      "title": "Time (Millisecond) vs Energy (Joule) - MobileNet vs AlexNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/34675581")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond Time",
      "type": "line"
    },
    [
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2 (MobileNet)",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "33471021",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/energy"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621 (MobileNet)",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "34675581",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/energy"
          })
      }, {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2 (AlexNet)",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "5980",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/energy"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621 (AlexNet)",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "3812459",
            "phase": "P3",
            "basedon": "power.draw",
            "route": "/canonicalOperation/primaryECC/energy"
          })
      }
    ])
  
  display_chart(
    {
      "title": "Time Read (Millisecond) vs Spent Time (Seconds) - MobileNet vs AlexNet (P3)",
      "labels": (await get_data("/dataChart/surveyModel/34675581")).run_plan.collector.list_frequency,
      "yLabel": "Power Average",
      "xLabel": "Millisecond Time",
      "type": "line"
    },
    [
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2 (MobileNet)",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "33471021",
            "phase": "P3",
            "basedon": "timestamp",
            "route": "/canonicalOperation/primaryECC/duration"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621 (MobileNet)",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "34675581",
            "phase": "P3",
            "basedon": "timestamp",
            "route": "/canonicalOperation/primaryECC/duration"
          })
      },
      {
        "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2 (AlexNet)",
        "borderColor": "rgba(77,166,253,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "5980",
            "phase": "P3",
            "basedon": "timestamp",
            "route": "/canonicalOperation/primatyECC/duration"
          })
      },
      {
        "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621 (AlexNet)",
        "borderColor": "rgba(235, 70, 52,0.90)",
        "backgroundColor": "transparent",
        "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
          {
            "key_survey_model": "3812459",
            "phase": "P3",
            "basedon": "timestamp",
            "route": "/canonicalOperation/primatyECC/duration"
          })
      }
    ])


}
main()
/*
display_line_chart([
  {
    "label": "GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2",
    "color": "rgba(77,166,253,0.90)",
    "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
    {
      "key_survey_model": "5980",
		  "phase": "P3",
		  "basedon": "power.draw",
		  "route": "/canonicalOperation/primaryECC/energy"
    })
  },
  {
    "label": "GPU-4f484041-b71d-74ad-ac68-de41c0da4621", "color": "rgba(235, 70, 52,0.90)",
    "data": await get_data_post("/dataChart/valueThisLevelOrFrequency",
    {
      "key_survey_model": "3812459",
		  "phase": "P3",
		  "basedon": "power.draw",
		  "route": "/canonicalOperation/primaryECC/energy"
    })
  }
])
*/
//display_chart_power_av_of_freq("5980", "20793470", "Read Frequency Power Average of GPU-b24b87b6-fce8-397d-5c35-2fb3bb4fdce2")
//display_chart_power_av_of_freq("3812459", "8922720", "Read Frequency Power Average of GPU-4f484041-b71d-74ad-ac68-de41c0da4621")
//display_chart_power_std()
//display_chart_power_av()

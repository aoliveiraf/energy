const joi = require('joi');


const schema = {
	list_phase : {
		"first": "P1",
		"last": "P4",
		"P1": {
			"next": "P2"
		},
		"P2": {
			"next": "P3"
		},
		"P3": {
			"next": "P4"
		}
	}
}

module.exports = schema;
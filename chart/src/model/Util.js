
module.exports = {
    csvToJSON: function (batch,list_property) {

        let list_JSON = []
        for (let i = 1; i < batch.length; i++) {
            const list_value = this.splitRowCSV(batch[i])

            let json = {};
            for (let j = 0; j < listValue.length; j++) {
                json[list_property[j]] = list_value[j];
            }
            list_JSON.push(json)
        }
        return list_JSON;

    },     
    semiCsvToJSON: function (batch,list_property) {

        let list_JSON = []

    
        for (let i = 0; i < batch.length; i++) {
            const list_value = this.splitRowCSV(batch[i])

            const split = list_value[0].split(" ");
            
            const timestamp = split[0];
            let second_property = split[1];
            
            let json = {};
            
            const d = new Date(timestamp);
            json.timestamp=d.getTime();
//            json["timestamp"] = d.getTime();
            json[list_property[0]] = second_property;
            for (let j = 1; j < list_value.length; j++) {
                
                json[list_property[j]] = this.extractOnlyValue(list_value[j]);
            }
            list_JSON.push(json)
        }
        return list_JSON;

    },   
    extractOnlyValue:function (value) {
        const split = value.split(" ");

        return split[0];
    }, 
    splitRowCSV: function (row) {
        let listContent = row.split(',');
        for(let i=0;i<listContent.length;i++) {
            listContent[i]=listContent[i].trim()
        }
        return listContent;
    }
}


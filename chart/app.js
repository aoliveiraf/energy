require('dotenv').config();
const express = require('express');
const index = require('./src/routes/index');
const path = require('path');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');

const util = require('./src/routes/util')

const dataChart = require('./src/routes/dataChart')


const app = express();

app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(morgan('dev'));


app.use('/',index)

app.use('/dataChart',dataChart);

app.use('/util',util);

app.listen(3001,()=>{
    console.log('listening...',3001);
})

